<?php
    $json_data= file_get_contents(__DIR__ . '/contacts_data.txt'); 
    $persons_contacts=json_decode($json_data, true);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body> 
    <h1 style="width: 800px; margin: 50px auto 40px; text-align:center">Контактные данные</h1>
   <table class="table" style="width: 800px; margin:auto">
    <thead>
     <tr>
      <th scope="col">Фамилия</th>
      <th scope="col">Имя</th>
      <th scope="col">Адресс</th>
      <th scope="col">Телефон</th>
      <th scope="col"></th>
     </tr>
   </thead>
    <?php
        foreach($persons_contacts as $person=> $person_details){
            echo '<tr>';
            echo '<td scope="col">'.$person_details["lastName"].'</td>';
            echo '<td scope="col">'.$person_details["firstName"] .'</td>';
            echo '<td scope="col">'.$person_details["address"] .'</td>';
            echo '<td scope="col">'.$person_details["phoneNumber"].'</td>';
            echo '</tr>';
        }
    ?>
    </table>
    </body>
</html>    
	


