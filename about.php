<?php
	$userName = 'Агния';
	$yearOfBirth = 1984;
	$currentYear = 2018;
	$userAge = $currentYear - $yearOfBirth;
	$userEmail = 'agness.agn@mail.ru'; 
	$userCity = 'Москва';
	$userIntro = 'работаю в ГКБ им. С.П. Боткина';	
?>
<h1>Страница пользователя <?= $userName; ?> </h1>
<table style="width:50%">
  <tr>
    <td>Имя</td>
    <td><?= $userName; ?></td>
  </tr>
  <tr>
    <td>Возраст</td>
    <td><?= $userAge; ?> </td>
  </tr>
  <tr>
    <td>Адрес электронной почты</td>
    <td><?= $userEmail; ?></td>
  </tr>
  <tr>
    <td>Город</td>
    <td> <?= $userCity; ?></td>
  </tr>
  <tr>
    <td>О себе</td>
    <td><?= $userIntro; ?></td>
  </tr>
</table>
