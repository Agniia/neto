<?php 
    if(!empty($_POST)&&array_key_exists('testName', $_POST)){
        $testName = $_POST['testName'];
    }
    if(!empty($_FILES)&&array_key_exists('testFile', $_FILES)){
        if (strpos($_FILES['testFile']['type'], 'json') !== false) {
            $number = count(glob(__DIR__ . '/tests/*.json'));
            $filename = $_FILES['testFile']['tmp_name'];
            $dist = __DIR__ . '/tests/'.$testName.'_test_'.$number.'_.json';
            move_uploaded_file($filename, $dist);
        }
     }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body> 
    <h1 style="width: 800px; margin: 50px auto 40px; text-align:center">Форма загрузки теста</h1>
    <form style="width: 400px; margin: 50px auto 40px; text-align:center" enctype="multipart/form-data" action="admin.php" method="post">                    
        <label style="margin: 20px 0 10px;" for="testName">Укажите название теста</label>  
        <input style="margin: 0 0 20px;" id="testName" class="form-control" name="testName" type="text" />
        <label style="margin: 20px 0 10px;" for="testFile">Загрузить файл с тестом (допустимый формат JSON)</label>  
        <input style="margin: 0 0 20px;" id="testFile" class="form-control" name="testFile" type="file" />
        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
    </body>
</html>    
	


