<?php
  $continents_animals = array(
     "Asia" => array('Ursus Thibetanus', 'Balaenoptera', 'Acinonyx Jubatus',' Alligator sinensis'),
     "Africa" => array('Atelerix albiventris', 'Lemur', 'Cynomys ludovicianus', ' Bubalus bubalis', 'Agama mwanzae'),
      "North America" => array('Sciurus aberti' , 'Lepus othus', 'Tamias', 'Mustela vison'),
      "South America" => array('Vicugna pacos', 'Callicebus', 'Inia geoffrensis', ' Trichechus inunguis'),
      "Antarctica" => array('Aptenodytes forsteri', 'Aptenodytes patagonica', 'Arctocephalus gazella','Mirounga'),
      "Europe" => array('Ursus arctos', 'Delphinidae', ' Bubo', 'Erinaceus europaeus'),
      "Australia" => array('Tachyglossus aculeatus', 'Dasyurus', 'Petaurus breviceps', 'Canis familiaris')
  );
  
  $two_words_animals = [];
  $first_part_name = [];
  $second_part_name = [];
  foreach($continents_animals as $continent => $animals ){
        foreach($animals as $animal){
            $animal_parts = explode(" ", trim($animal));
            if(count($animal_parts) == 2){
                 $two_words_animals[] =  $animal;
                 $first_part_name[] = $animal_parts[0];
                 $second_part_name[] = $animal_parts[1];
            }
        }
 }
$new_animals = [];
$limiter = count($first_part_name);
for($i = 0; $i < $limiter; $i++){
    $random_number_1 = rand(0, $limiter-1);
    $random_number_2 = rand(0, $limiter-1);
    $new_animals[] = $first_part_name[$random_number_1] .' '.$second_part_name[$random_number_2];
}
echo 'Old animals'; 
echo '<br>';
foreach($two_words_animals as $two_words_animal){
     var_dump($two_words_animal); 
     echo '<br>';
}
echo '<br>';
echo 'New animals';      
echo '<br>';
foreach($new_animals as $new_animal){
     var_dump($new_animal); 
     echo '<br>';
}
?>
