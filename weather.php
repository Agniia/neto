<?php
$European_capitals = array(
    "Albania" => "Tirana",
    "Andorra" => "Andorra la Vella",
    "Armenia" => "Yerevan",
    "Austria" => "Vienna",
    "Azerbaijan" => "Baku",
    "Belarus" => "Minsk",	    
    "Belgium" => "Brussels",
    "Bosnia & Herzegovina" => "Sarajevo",
    "Bulgaria" => "Sofia",
    "Croatia" => "Zagreb",
    "Czech Republic" => "Prague",	
     "Denmark" => "Copenhagen",  
    "Estonia" => "Tallinn",   
    "Finland" => "Helsinki",  
    "France" => "Paris",  
    "Georgia" => "Tbilisi",
    "Germany" => "Berlin",	    
    "Greece" => "Athens",
    "Hungary" => "Budapest",
    "Iceland" => "Reykjavik",
    "Ireland" => "Dublin",
    "Italy" => "Rome",
    "Latvia" => "Riga",	
    "Liechtenstein" => "Vaduz",	
    "Lithuania" => "Vilnius",	
    "Luxembourg" => "Luxembourg",	
    "Macedonia" => "Skopje",	
     "Malta" => "Valletta",	
    "Moldova" => "Chisinau",	
     "Monaco" => "Monaco-Ville",	
     "Montenegro" => "Podgorica",	
     "The Netherlands" => "Amsterdam",	
     "Norway" => "Oslo",	
     "Poland" => "Warsaw",	
     "Portugal" => "Lisbon",
    "Romania" => "Bucharest",
     "Russia" => "Moscow",
     "Serbia" => "Belgrade",
     "Slovakia" => "Bratislava",
      "Slovenia" => "Ljubljana",
      "Spain" => "Madrid",
     "Sweden" => "Stockholm",
     "Turkey" => "Ankara",    
     "Ukraine" => "Kiev",    
      "United Kingdom" => "London"
);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body> 
    <h1 style="width: 800px; margin: 50px auto 40px; text-align:center">Weather in European capitals</h1>
   <table class="table" style="width: 800px; margin:auto">
    <thead>
     <tr>
      <th scope="col">Country</th>
      <th scope="col">Capital</th>
      <th scope="col">Temperature</th>
      <th scope="col">Weather description</th>
      <th scope="col"></th>
     </tr>
   </thead>
    <?php
        foreach($European_capitals as $European_country => $European_capital){
            echo '<tr>';
            echo '<td scope="col">'.$European_country .'</td>';
            echo '<td scope="col">'.$European_capital .'</td>';
                $json = file_get_contents('https://api.openweathermap.org/data/2.5/weather?q='.$European_capital.'&units=metric&appid=6ad177a43e99f2dc847c3d43c74f5b8b');
                $obj = json_decode($json);
            echo '<td scope="col">'.$obj->main->temp.' C&#176; </td>';
            echo '<td scope="col">'.$obj->weather[0]->description.' </td>';
            echo '<td scope="col"><img src="http://openweathermap.org/img/w/'.$obj->weather[0]->icon.'.png"></td>';
            echo '</tr>';
        }
    ?>
    </table>
    </body>
</html>    
	


